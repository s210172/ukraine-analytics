#############################################################################################################
### TEXT PROCESSING
#############################################################################################################

import string
import re
from nltk.stem import WordNetLemmatizer
lemmatizer = WordNetLemmatizer()
from country_list import countries_for_language

## Stops words
from sklearn.feature_extraction import _stop_words
new_stops=['http', 'https', 'putin', 'ukraine', 'ukriane' 'ukrainian', 'russia', 'russian', 'tv', 'channel']
stop_words= _stop_words.ENGLISH_STOP_WORDS
STOP_WORDS = stop_words.union(set(new_stops))

## Process hashtags
# text = "#fightForUkraine  #whataboutNow? is a very good. #resistRussia again in nowadays #putinDown is some"
# https://stackoverflow.com/questions/68448243/efficient-way-to-split-multi-word-hashtag-in-python
def process_en_hashtag(input_text: str) -> str: 
    return re.sub( r'#[a-z]\S*', 
                  lambda m: ' '.join(re.findall('[A-Z][^A-Z]*|[a-z][^A-Z]*',
                                     m.group().lstrip('#'))), input_text, )

## Regex
reg_username = r"@([^\s]+)" # regex username - all words that start with '@'
reg_http =  r"http([^\s]+)"

## Delete emojis
def deEmojify(x):
    regrex_pattern = re.compile(pattern = "["
        u"\U0001F600-\U0001F64F"  # emoticons
        u"\U0001F300-\U0001F5FF"  # symbols & pictographs
        u"\U0001F680-\U0001F6FF"  # transport & map symbols
        u"\U0001F1E0-\U0001F1FF"  # flags (iOS)
                           "]+", flags = re.UNICODE)
    return regrex_pattern.sub(r'', x)

## Removing URL
# credits to: https://www.kaggle.com/bwandowando/ukraine-russian-crisis-twitter-dataset-1-2-m-rows
def remove_urls(x):
    cleaned_string = re.sub(r'(https|http)?:\/\/(\w|\.|\/|\?|\=|\&|\%)*\b', '', str(x), flags=re.MULTILINE)
    return cleaned_string


## Text processing
def text_processing(text):
    """
    @params
        text: string -> text to process
    @outcome
        cleaned text
    """
    # remove urls
    text = remove_urls(text)
    # split multi-word hashtags
#     text = process_en_hashtag(text)
    # remove whitespaces
    text.strip()
    # remove usernames
    text = " ".join([w for w in text.split() if bool(re.match(reg_username, w)) == False])
    
    # remove emojis
    text = deEmojify(text)
    # keep only text
#     ????????????????????
    text = " ".join([re.sub(r'[^A-Za-z ]+', '', w) for w in text.split()])
#      ????????????????????
    # lowercase
#     text = "".join([c.lower() for c in text])
    # remove '#' from hashtags
    text = ' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)"," ", text).split())
    # remove punctuation 
    text = "".join([c for c in text if c not in string.punctuation])
    # stemming / lematizing (optional)
#     text = " ".join([lemmatizer.lemmatize(w) for w in text.split()])
    # remove stopwords
#     text = " ".join([w for w in text.split() if w not in STOP_WORDS])
    
    return text


def clean_text(text):
    text = " ".join(([re.sub("[()]", " ", t) for t in text.split()
                        if re.match(r'[^\W\d]*$', re.sub("[()]", "", t))]))
    return text
    
  
## Setting up the function to extract country name from text
countries = pd.DataFrame()
countries['name'] = [text_processing(c.name) for c in pycountry.countries]
countries['abbr'] = [text_processing(c.alpha_3) for c in pycountry.countries]

import re
try:
    import pycountry 
except ModuleNotFoundError:
    !pip install pycountry
    

def get_country(text):
    words = text.split()
    for w in words:
        found = False
        if w in list(countries['name']):
            found = True
            return(w) 
        elif w in list(countries['abbr']) and not found:
            return countries[countries['abbr'] == w]['name'].values[0]
    return('No')
